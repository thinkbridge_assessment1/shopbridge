package com.shopbridge.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.shopbridge.model.Product;

public interface ProductRepository  extends JpaRepository<Product, Integer> {

}
